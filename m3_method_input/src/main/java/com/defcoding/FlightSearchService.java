package com.defcoding;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.format.DateTimeFormatter.ofPattern;

public class FlightSearchService {

    FlightStore flightStore;

    public FlightSearchService(FlightStore flightStore) {
        this.flightStore = flightStore;
    }

    public List<Flight> search(String fromDest, String toDest, String departDate, int passengerNum) {

        if(isNotValidString(fromDest) ||
           isNotValidString(toDest) ||
           isNotValidString(departDate)){
           String msg = String.format("You have provided the following arguments, none of them can be null. " +
                    "fromDest: %s, toDest: %s, date: %s", fromDest, toDest, departDate);
           throw new IllegalArgumentException(msg);
        }

        if(fromDest.equalsIgnoreCase(toDest)){
            throw new IllegalArgumentException("From and To Destinations cannot be the same. " +
                    "You input " + fromDest);
        }

        LocalDate date = parseInputDate(departDate);

        if(passengerNum < 1 || passengerNum >7){
            throw new IllegalArgumentException("The number of passenger must be between 1 to 7");
        }

        List<Flight> flights = flightStore.getFlights();

        return flights.stream()
                    .filter(f -> f.getFromDest().equals(fromDest))
                    .filter(f -> f.getToDest().equals(toDest))
                    .filter(f -> date.equals(LocalDate.parse(f.getDate(), ofPattern("dd-MM-yyyy"))))
                    .filter(f -> f.getSeatsAvailable() >= passengerNum)
                    .collect(Collectors.toList());
    }

    private boolean isNotValidString(String s){
        return s == null || s.trim().isEmpty();
    }

    private LocalDate parseInputDate(String departDate) {
        LocalDate date;

        try {
            date = LocalDate.parse(departDate, ofPattern("dd-MM-yyyy"));
        } catch (DateTimeParseException exception){
            throw new IllegalArgumentException(String.format("Could not parse input date %s, " +
                    "please input a date in format dd-MM-yyyy", departDate));
        }
        return date;
    }

    public static FlightSearchService flightSearch(){
        return new FlightSearchService(new FlightStoreImpl());
    }


}
